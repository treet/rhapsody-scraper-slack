const { WebClient } = require("@slack/web-api");
const { fetchLunchMenu } = require("@treet/rhapsody-scraper");

const web = new WebClient(process.env.SLACK_TOKEN);

(async () => {
  try {
    const week = await fetchLunchMenu();
    const today = new Date().getDay();
    const menu = week[today - 1];

    if (menu != null) {
      await web.chat.postMessage({
        channel: "#random",
        text: menu.map((entry) => `- ${entry}\n`).join(""),
      });
    }
  } catch (error) {
    console.log(error);
  }
})();
